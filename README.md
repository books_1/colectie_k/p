# P

## Content

```
./P. B. Kerr:
P. B. Kerr - Copiii lampii fermecate - V1 Akhnaton si djinnii captivi 1.0 '{AventuraTineret}.docx
P. B. Kerr - Copiii lampii fermecate - V2 Babilonul djinnului albastru 1.0 '{AventuraTineret}.docx
P. B. Kerr - Copiii lampii fermecate - V3 Cobra, regele din Katmandu 1.0 '{AventuraTineret}.docx
P. B. Kerr - Copiii lampii fermecate - V4 Djinnii razboinici 1.0 '{AventuraTineret}.docx
P. B. Kerr - Copiii lampii fermecate - V5 Enigma portalului blestemat 1.0 '{AventuraTineret}.docx

./P. C. Wren:
P. C. Wren - Beau Geste 1.0 '{ActiuneComando}.docx

./P. D. Ouspensky:
P. D. Ouspensky - A patra cale - Gurdjieff 1.0 '{Spiritualitate}.docx

./P. J. Tracy:
P. J. Tracy - Monkeewrench - V1 Operatiunea Monkeewrench 1.0 '{Politista}.docx
P. J. Tracy - Monkeewrench - V2 Momeala vie 1.0 '{Politista}.docx

./P. Stanescu:
P. Stanescu - Intamplare obisnuita 1.0 '{Diverse}.docx

./P. Z. Reizin:
P. Z. Reizin - Algoritmul fericirii 2.0 '{Literatura}.docx

./Paisie Aghioratul:
Paisie Aghioratul - Cuvinte Duhovnicesti - V1 Cu durere si dragoste pentru omul contemporan 1.0 '{Religie}.docx
Paisie Aghioratul - Cuvinte Duhovnicesti - V2 Trezire duhovniceasca 1.0 '{Religie}.docx
Paisie Aghioratul - Cuvinte Duhovnicesti - V3 Nevointa duhovniceasca 1.0 '{Religie}.docx
Paisie Aghioratul - Cuvinte Duhovnicesti - V4 Viata de familie 1.0 '{Religie}.docx
Paisie Aghioratul - Cuvinte Duhovnicesti - V5 Patimi si virtuti 1.0 '{Religie}.docx
Paisie Aghioratul - Cuvinte Duhovnicesti - V6 Despre rugaciune 1.0 '{Religie}.docx
Paisie Aghioratul - Viata de familie 0.9 '{Religie}.docx

./Palacio Valdes:
Palacio Valdes - Sora san sulpicio 1.0 '{Literatura}.docx

./Palmer Michael:
Palmer Michael - A cincea fiola 2.0 '{Suspans}.docx

./Pamela Kent:
Pamela Kent - Dragoste la Istambul 0.99 '{Romance}.docx
Pamela Kent - Dragoste si santaj 0.8 '{Dragoste}.docx
Pamela Kent - Incantatia dragostei 0.99 '{Dragoste}.docx

./Pamela L. Travers:
Pamela L. Travers - Mary Poppins si casa de alaturi 0.8 '{Copii}.docx

./Pamela Macaluso:
Pamela Macaluso - Trandafir printre spini 0.99 '{Dragoste}.docx

./Pamela Toth:
Pamela Toth - Destinati unul celuilalt 0.99 '{Dragoste}.docx
Pamela Toth - Femeia ideala 0.99 '{Dragoste}.docx

./Pam Jenoff:
Pam Jenoff - Fetele disparute din Paris 1.0 '{Suspans}.docx
Pam Jenoff - Povestea unui orfan 1.0 '{Literatura}.docx

./Pam Munoz Ryan:
Pam Munoz Ryan - Picteaza vantul 1.0 '{Literatura}.docx
Pam Munoz Ryan - Trandafirii din Mexic 1.0 '{Literatura}.docx

./Panait Istrati:
Panait Istrati - Adolescenta lui Adrian Zografi - Mihail 2.0 '{ClasicRo}.docx
Panait Istrati - Amintiri. Evocari. Confesiuni 1.0 '{ClasicRo}.docx
Panait Istrati - Biroul de plasare 1.0 '{ClasicRo}.docx
Panait Istrati - Casa Thuringer 1.0 '{ClasicRo}.docx
Panait Istrati - Chira Chiralina 1.0 '{ClasicRo}.docx
Panait Istrati - Ciulinii baraganului 1.0 '{ClasicRo}.docx
Panait Istrati - Copilaria lui Adrian Zografi - Codin 1.0 '{ClasicRo}.docx
Panait Istrati - Copilaria lui Adrian Zografi - Kir Nicola 1.0 '{ClasicRo}.docx
Panait Istrati - Copilaria lui Adrian Zografi - O noapte in balti 1.0 '{ClasicRo}.docx
Panait Istrati - Cum am devenit scriitor 1.0 '{ClasicRo}.docx
Panait Istrati - Domnita din Snagov 1.0 '{ClasicRo}.docx
Panait Istrati - Dragomir 0.99 '{ClasicRo}.docx
Panait Istrati - Familia Perlmutter 1.0 '{ClasicRo}.docx
Panait Istrati - Haiducii 2.0 '{ClasicRo}.docx
Panait Istrati - In lumea Mediteranei - Apus de soare 0.9 '{ClasicRo}.docx
Panait Istrati - In lumea Mediteranei - Rasarit de soare 1.0 '{ClasicRo}.docx
Panait Istrati - Mos Anghel. Tata Minca 1.0 '{ClasicRo}.docx
Panait Istrati - Nerantula si alte povestiri 1.0 '{ClasicRo}.docx
Panait Istrati - Pelerinul inimii 0.5 '{ClasicRo}.docx
Panait Istrati - Pentru a fi iubit Pamantul 0.6 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V1 La stapan 0.9 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V3 Capitan Mavromati 0.9 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V7 Moravuri literaro-gazetaresti 0.8 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V9 Pescuitorul de bureti 0.9 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V10 Bakir 0.9 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V11 Intre un prieten si o tutungerie 0.9 '{ClasicRo}.docx
Panait Istrati - Primii Pasi spre Lupta - V12 Sotir 0.99 '{ClasicRo}.docx
Panait Istrati - Spovedanie pentru invinsi 1.0 '{ClasicRo}.docx
Panait Istrati - Stavru 0.99 '{ClasicRo}.docx
Panait Istrati - Tata Minca 0.9 '{ClasicRo}.docx
Panait Istrati - Trecut si viitor 0.9 '{ClasicRo}.docx
Panait Istrati - Viata lui Adrian Zografi 1.0 '{ClasicRo}.docx

./Panait Munteanu:
Panait Munteanu - Povestea vietii mele 0.8 '{Biografie}.docx

./Paola Santagostino:
Paola Santagostino - Cum sa te vindeci cu o poveste 0.8 '{Sanatate}.docx

./Paolo Bacigalupi:
Paolo Bacigalupi - Fata modificata 1.0 '{SF}.docx
Paolo Bacigalupi - Omul calorie 1.0 '{SF}.docx
Paolo Bacigalupi - Omul cu cartela galbena 1.0 '{SF}.docx

./Paolo Giordano:
Paolo Giordano - Corpul uman 0.9 '{ActiuneRazboi}.docx
Paolo Giordano - Singuratatea numerelor prime 1.0 '{Diverse}.docx

./Paolo Monelli:
Paolo Monelli - O aventura in secolul intai V1 1.0 '{AventuraIstorica}.docx
Paolo Monelli - O aventura in secolul intai V2 1.0 '{AventuraIstorica}.docx

./Paramhansa Yogananda:
Paramhansa Yogananda - Autobiografia unui yoghin 0.9 '{Spiritualitate}.docx

./Parinoush Saniee:
Parinoush Saniee - Cel care ma asteapta 0.8 '{Literatura}.docx
Parinoush Saniee -Tatal celuilalt copil 1.0 '{Literatura}.docx

./Parisoula Lampsos & Lena Katarina Swanberg:
Parisoula Lampsos & Lena Katarina Swanberg - Viata mea cu Saddam Hussein 1.0 '{Literatura}.docx

./Par Lagerkvist:
Par Lagerkvist - Baraba 0.99 '{Literatura}.docx
Par Lagerkvist - Moartea lui Ahasverus 0.9 '{Literatura}.docx
Par Lagerkvist - Piticul 0.9 '{Literatura}.docx
Par Lagerkvist - Povestiri amare 1.0 '{Literatura}.docx

./Pascal Bruckner:
Pascal Bruckner - Capcaunii anonimi 2.0 '{Thriller}.docx
Pascal Bruckner - Care dintre noi l-a nascocit pe celalalt 2.0 '{Thriller}.docx
Pascal Bruckner - Copilul divin 2.0 '{Thriller}.docx
Pascal Bruckner - Hotii de frumusete 4.0 '{Thriller}.docx
Pascal Bruckner - Iubirea fata de aproapele 2.0 '{Thriller}.docx
Pascal Bruckner - Iubito, eu ma micsorez 2.0 '{Thriller}.docx
Pascal Bruckner - Luni de fiere 4.0 '{Thriller}.docx
Pascal Bruckner - Palatul chelfanelii 2.0 '{Thriller}.docx
Pascal Bruckner - Paria 2.0 '{Thriller}.docx
Pascal Bruckner - Pazea, se-ntoarce Mos Craciun! 2.0 '{Thriller}.docx

./Pascal Bruckner & Alain Finkielkraut:
Pascal Bruckner & Alain Finkielkraut - Noua dezordine amoroasa 0.99 '{Psihologie}.docx

./Pascal Quignard:
Pascal Quignard - Sexul si spaima 0.8 '{Istorie}.docx
Pascal Quignard - Terasa la Roma 0.99 '{Diverse}.docx
Pascal Quignard - Toate diminetile lumii 0.9 '{Diverse}.docx

./Pat Barker:
Pat Barker - Tacerea femeilor 1.0 '{Literatura}.docx

./Pat Bucheister:
Pat Bucheister - Secretele trecutului 1.0 '{Romance}.docx

./Pat Murphy:
Pat Murphy - Vorbeam din adancuri 0.99 '{SF}.docx

./Patrice van Eersel & Catherine Halliard:
Patrice van Eersel & Catherine Halliard - Ma dor stramosii 1.0 '{Psihologie}.docx

./Patricia Cornwell:
Patricia Cornwell - Cartea mortilor 1.0 '{Politista}.docx
Patricia Cornwell - Indiciu fatal 1.0 '{Politista}.docx
Patricia Cornwell - Ratiuni criminale 1.0 '{Politista}.docx

./Patricia Highsmith:
Patricia Highsmith - Carol 1.0 '{Literatura}.docx
Patricia Highsmith - Va rog, nu trageti in pomi 0.99 '{Literatura}.docx

./Patricia Hill:
Patricia Hill - Misterul ochilor tai 0.99 '{Dragoste}.docx
Patricia Hill - Prinsa in mrejele pasiunii 0.99 '{Romance}.docx
Patricia Hill - Sian 0.9 '{Dragoste}.docx

./Patricia Lynn:
Patricia Lynn - Casatorie prin internet 0.9 '{Dragoste}.docx
Patricia Lynn - Comportament dubios 0.99 '{Dragoste}.docx
Patricia Lynn - Undeva departe 0.99 '{Dragoste}.docx

./Patricia Melo:
Patricia Melo - Ucigasul 1.0 '{Literatura}.docx

./Patricia Muse:
Patricia Muse - Misterul din turn 0.9 '{Romance}.docx

./Patricia Nann:
Patricia Nann - Ochi de sarpe 1.0 '{Romance}.docx

./Patricia Neal:
Patricia Neal - Un american la Londra 0.99 '{Romance}.docx

./Patricia Olney:
Patricia Olney - Fidelitate rasplatita 0.99 '{Romance}.docx
Patricia Olney - Riscul lui Jade 0.99 '{Dragoste}.docx

./Patricia Potter:
Patricia Potter - Drum necunoscut 0.99 '{Dragoste}.docx
Patricia Potter - Libertatea tatalui 0.9 '{Dragoste}.docx

./Patricia Randall:
Patricia Randall - Totul pentru a placea 0.99 '{Romance}.docx

./Patricia Vandenberg:
Patricia Vandenberg - Jesica isi doreste un tatic 0.99 '{Dragoste}.docx
Patricia Vandenberg - N-ai voie sa-ti pierzi curajul 0.99 '{Dragoste}.docx
Patricia Vandenberg - Raza de soare 0.7 '{Dragoste}.docx

./Patricia Wilson:
Patricia Wilson - Adapost periculos 0.9 '{Romance}.docx

./Patricia Wood:
Patricia Wood - Loterie 1.0 '{Literatura}.docx

./Patrick Braun:
Patrick Braun - Comorile incasilor 1.0 '{Civilizatii}.docx

./Patrick Calinescu:
Patrick Calinescu - Nouazeci si noua si ceva 0.99 '{ProzaScurta}.docx
Patrick Calinescu - Palma 0.99 '{ProzaScurta}.docx
Patrick Calinescu - Repetabilul irepetabil 0.99 '{ProzaScurta}.docx
Patrick Calinescu - Web & Gutenberg 0.99 '{ProzaScurta}.docx

./Patrick Carman:
Patrick Carman - Atherton - Casa puterii 1.0 '{AventuraTineret}.docx

./Patrick Girard:
Patrick Girard - Romanul Cartaginei - V1 Hamilcar, leul desertului 0.99 '{AventuraIstorica}.docx

./Patrick Graham:
Patrick Graham - Evanghelia dupa satana 1.0 '{Horror}.docx

./Patrick Lemoine:
Patrick Lemoine - Misterul Nocebo 0.99 '{MistersiStiinta}.docx

./Patrick Modiano:
Patrick Modiano - Calatorie de nunta 1.0 '{Literatura}.docx
Patrick Modiano - Micuta Bijou 0.8 '{Literatura}.docx
Patrick Modiano - Strada dughenelor intunecoase 1.0 '{Politista}.docx

./Patrick Ness:
Patrick Ness - Chemarea monstrului 1.0 '{Tineret}.docx
Patrick Ness - Pe Taramul Haosului - V1 Vocea pumnalului 1.0 '{Tineret}.docx
Patrick Ness - Pe viata si pe moarte 0.9 '{Tineret}.docx

./Patrick Raveau:
Patrick Raveau - Memoria vantului 0.99 '{Diverse}.docx

./Patrick Rothfuss:
Patrick Rothfuss - Cronicile Ucigasului de Regi - V1 Numele vantului 2.0 '{SF}.docx
Patrick Rothfuss - Cronicile Ucigasului de Regi - V2 Teama inteleptului 2.0 '{SF}.docx
Patrick Rothfuss - Tainica iscodire a lucrurilor tacute 1.0 '{SF}.docx

./Patrick Suskind:
Patrick Suskind - Parfumul 2.0 '{Literatura}.docx

./Patrick Voicu:
Patrick Voicu - Ultima scrisoare 0.99 '{Teatru}.docx

./Patti Beckman:
Patti Beckman - Barca visurilor 0.9 '{Dragoste}.docx

./Patty Copeland:
Patty Copeland - Casatorie de convenienta 0.99 '{Dragoste}.docx
Patty Copeland - Numai despre Eve 0.99 '{Dragoste}.docx
Patty Copeland - Punctaj final 0.9 '{Dragoste}.docx

./Pat Van Wie:
Pat Van Wie - Iubind-o pe Lindsey 0.99 '{Dragoste}.docx

./Pat West:
Pat West - Intalnire in safari 0.9 '{Romance}.docx
Pat West - Mireasa potrivita 0.9 '{Dragoste}.docx
Pat West - Un barbat neindurator 0.99 '{Romance}.docx

./Paula Firth:
Paula Firth - Cecul fara acoperire 0.8 '{Dragoste}.docx
Paula Firth - Inima bine pazita 0.99 '{Dragoste}.docx

./Paula Hawkins:
Paula Hawkins - Fata din tren 1.0 '{Thriller}.docx
Paula Hawkins - In ape adanci 1.0 '{Literatura}.docx

./Paula Helmer:
Paula Helmer - Doar un sarut 0.99 '{Dragoste}.docx
Paula Helmer - Imbarcare clandestina 0.99 '{Romance}.docx
Paula Helmer - Proba focului 0.99 '{Dragoste}.docx

./Paula McLain:
Paula McLain - Sotia din Paris 0.99 '{Literatura}.docx

./Paul Anghel:
Paul Anghel - Iesirea din iarna 1.0 '{Literatura}.docx

./Paul Antim:
Paul Antim - Balaban si statuia 1.0 '{Politista}.docx
Paul Antim - Disparitia unui contabil 1.0 '{Politista}.docx
Paul Antim - Un Casanova calatoreste spre iad 1.0 '{Politista}.docx

./Paul Auster:
Paul Auster - Cartea iluziilor 0.9 '{Literatura}.docx
Paul Auster - In tara ultimelor lucruri 2.0 '{Literatura}.docx
Paul Auster - Trilogia New York-ului 0.99 '{Literatura}.docx

./Paul B. Thompson:
Paul B. Thompson - In inima rece a Soarelui 1.0 '{SF}.docx

./Paul Beatty:
Paul Beatty - Io contra Statele Unite ale Americii 1.0 '{Literatura}.docx
Paul Beatty - Winston durul 1.0 '{Literatura}.docx

./Paul Berben & Bernard Iselin:
Paul Berben & Bernard Iselin - Remagen. Podul sansei 1.0 '{Razboi}.docx

./Paul Boncutiu:
Paul Boncutiu - Intre ei si noi, Pamantul! V2 0.9 '{SF}.docx

./Paul Bonnecarrere:
Paul Bonnecarrere - Indraznetii inving V1 1.0 '{ActiuneComando}.docx
Paul Bonnecarrere - Indraznetii inving V2 1.0 '{ActiuneComando}.docx

./Paul Bourget:
Paul Bourget - Inima de femeie 1.0 '{Dragoste}.docx

./Paul Brickhill:
Paul Brickhill - Bader invingatorul cerului 2.0 '{ActiuneRazboi}.docx

./Paul Brunton:
Paul Brunton - Egiptul secret 0.99 '{Civilizatii}.docx

./Paul C. Reisser:
Paul C. Reisser - Sexul si persoanele necasatorite 0.9 '{Psihologie}.docx

./Paul Constant:
Paul Constant - Iancu Jianu 1.0 '{IstoricaRo}.docx

./Paul Daian:
Paul Daian - Stangacia in salut a femeii 1.0 '{Versuri}.docx

./Paul Di Filippo:
Paul Di Filippo - Sisif si strainul 1.0 '{SF}.docx

./Paul Donnelley:
Paul Donnelley - Asasini si asasinate care au schimbat istoria lumii 1.0 '{Istorie}.docx

./Paul Doru Mugur:
Paul Doru Mugur - Cucu 0.9 '{ProzaScurta}.docx
Paul Doru Mugur - Digital dreams 0.99 '{ProzaScurta}.docx
Paul Doru Mugur - Psihonautul 0.99 '{ProzaScurta}.docx
Paul Doru Mugur - Radiohead 0.99 '{ProzaScurta}.docx
Paul Doru Mugur - Sindromul Pessoa 0.99 '{ProzaScurta}.docx
Paul Doru Mugur - Testul Turing 0.99 '{ProzaScurta}.docx
Paul Doru Mugur - Zerlendi@shambala 0.9 '{Spiritualitate}.docx

./Paul Dowswell:
Paul Dowswell - Auslander 1.0 '{Literatura}.docx

./Paul Erdman:
Paul Erdman - Filiera elvetiana 1.0 '{Thriller}.docx
Paul Erdman - Panica din 89 1.0 '{Thriller}.docx
Paul Erdman - Sub semnul riscului 1.0 '{Thriller}.docx
Paul Erdman - Ultimele zile ale Americii 1.0 '{Thriller}.docx

./Paul Eugen Banciu:
Paul Eugen Banciu - Casa Ursei Mari 0.9 '{Literatura}.docx
Paul Eugen Banciu - Exercitii de exil interior 0.9 '{Literatura}.docx
Paul Eugen Banciu - Luna neagra 0.99 '{Literatura}.docx
Paul Eugen Banciu - O calatorie cu aeroplanul 0.99 '{Literatura}.docx
Paul Eugen Banciu - Remora jucatorul 0.99 '{Literatura}.docx
Paul Eugen Banciu - Somonul rosu 0.9 '{Literatura}.docx

./Paul Everac:
Paul Everac - Cadoul 0.99 '{Teatru}.docx
Paul Everac - Costache si viata interioara 1.0 '{Teatru}.docx
Paul Everac - Ferestre deschise 1.0 '{Teatru}.docx

./Paul Ferrini:
Paul Ferrini - Iubire fara conditii 0.99 '{Spiritualitate}.docx
Paul Ferrini - Linistea inimii V2 0.7 '{Spiritualitate}.docx

./Paul Feval:
Paul Feval - Arma nevazuta 1.0 '{CapasiSpada}.docx
Paul Feval - Capitanul Simon 1.0 '{CapasiSpada}.docx
Paul Feval - Cavalerul Mystere 1.0 '{CapasiSpada}.docx
Paul Feval - Cersetorul negru 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V1 Tineretea cocosatului 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V2 Cocosatul 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V3 Cavalcadele lui Lagardere 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V4 Mariquita 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V5 Triumful dragostei '1.0 si Spada}.docx
Paul Feval - Cocosatul - V6 Sergentul Belle Epee 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V7 Ducele de Nevers 1.0 '{CapasiSpada}.docx
Paul Feval - Cocosatul - V8 Gemenii lui Nevers 1.0 '{CapasiSpada}.docx
Paul Feval - D'artagnan contra Cyrano 1.0 '{CapasiSpada}.docx
Paul Feval - Fiul diavolului V1 1.0 '{CapasiSpada}.docx
Paul Feval - Fiul diavolului V2 1.0 '{CapasiSpada}.docx
Paul Feval - Fiul lui D'artagnan 0.9 '{CapasiSpada}.docx
Paul Feval - Lupul alb 1.0 '{CapasiSpada}.docx
Paul Feval - S2 - Black Coats - V2 Misterele Londrei V1 1.0 '{CapasiSpada}.docx
Paul Feval - S2 - Black Coats - V2 Misterele Londrei V2 1.0 '{CapasiSpada}.docx
Paul Feval - S2 - Black Coats - V4 Banditii Londrei 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V1 Manusa de otel 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V2 Inima de otel 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V3 Turnul crimei 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V4-5 Secretul fracurilor negre 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V6 Inghititorul de sabii 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V7 Cavalerii tezaurului 1.0 '{CapasiSpada}.docx
Paul Feval - S3 - Habits Noirs - V8 Banda Cadet 1.0 '{CapasiSpada}.docx
Paul Feval - Ultimele zane 1.0 '{CapasiSpada}.docx

./Paul Gadenne:
Paul Gadenne - Siloam 1.0 '{Dragoste}.docx

./Paul Goma:
Paul Goma - Alfabecedar 0.9 '{Literatura}.docx
Paul Goma - Altina 0.8 '{Necenzurat}.docx
Paul Goma - Arta refugii 0.8 '{Literatura}.docx
Paul Goma - Astra 0.9 '{Literatura}.docx
Paul Goma - Bonifacia 0.7 '{Literatura}.docx
Paul Goma - Doua reclamatii 0.8 '{Literatura}.docx
Paul Goma - Profil 0.8 '{Literatura}.docx
Paul Goma - Scrisoare deschisa 0.9 '{Literatura}.docx
Paul Goma - Scrisoare despre Plesita 0.8 '{Literatura}.docx
Paul Goma - Soldatul cainelui 0.9 '{Literatura}.docx
Paul Goma - Sus in varful raiului 0.8 '{Literatura}.docx
Paul Goma - Un fals pentru eternitate 0.8 '{Literatura}.docx

./Paul Guimard:
Paul Guimard - Strada Le Havre 0.7 '{Diverse}.docx

./Paul Hauck:
Paul Hauck - Cum sa iubesti pentru a fi iubit 0.9 '{Psihologie}.docx

./Paul Henricks:
Paul Henricks - Musuroiul de furnici 1.0 '{Politista}.docx

./Paul Hoffman:
Paul Hoffman - Mana Stanga a lui Dumnezeu - V1 Mana stanga a lui Dumnezeu 1.0 '{Supranatural}.docx
Paul Hoffman - Mana Stanga a lui Dumnezeu - V2 Ultimele patru lucruri 1.0 '{Supranatural}.docx
Paul Hoffman - Mana Stanga a lui Dumnezeu - V3 In bataia aripilor 1.0 '{Supranatural}.docx

./Pauline Gedge:
Pauline Gedge - Hatsepsut, o regina pentru eternitate 0.7 '{AventuraIstorica}.docx

./Pauline Peters:
Pauline Peters - Victoria Bredon - V1 Camera rubinie 1.0 '{Literatura}.docx

./Pauline Reage:
Pauline Reage - Povestea lui O 1.0 '{Erotic}.docx

./Paulin Lecca:
Paulin Lecca - Pelerinul rus 1.0 '{Spiritualitate}.docx

./Paul Johnson:
Paul Johnson - Intelectualii 1.0 '{Filozofie}.docx
Paul Johnson - Viata lui Isus povestita de un credincios 1.0 '{Spiritualitate}.docx

./Paul Jordan Smith:
Paul Jordan Smith - Povestirile Caissei 1.0 '{SF}.docx

./Paul Kennedy:
Paul Kennedy - Ascensiunea si decaderea marilor puteri 1.1 '{Istorie}.docx

./Paul Lacatus:
Paul Lacatus - Stranutand spre Utopia 0.99 '{Diverse}.docx

./Paul Lemerle:
Paul Lemerle - Istoria Bizantului 0.99 '{Istorie}.docx

./Paul Levine:
Paul Levine - Solomon vs Lord - V1 Solomon vs Lord 1.0 '{Thriller}.docx
Paul Levine - Solomon vs Lord - V2 Un alibi in larg 1.0 '{Thriller}.docx
Paul Levine - Solomon vs Lord - V3 Sa-i omoram pe toti avocatii 1.0 '{Thriller}.docx

./Paul Liekens:
Paul Liekens - Efectul de piramida 0.9 '{MistersiStiinta}.docx

./Paul Mackendrick:
Paul Mackendrick - Pietrele dacilor vorbesc 0.99 '{Istorie}.docx

./Paul McAuley:
Paul McAuley - Confluenta - V1 Copilul fluviului 1.0 '{SF}.docx
Paul McAuley - Razboiul linistit 1.0 '{SF}.docx

./Paul McKenna:
Paul McKenna - O viata noua in 7 zile 1.0 '{DezvoltarePersonala}.docx

./Paul Monette:
Paul Monette - Predator 1.0 '{SF}.docx

./Paulo Coelho:
Paulo Coelho - Adulter 1.0 '{Literatura}.docx
Paulo Coelho - Alchimistul 1.0 '{Literatura}.docx
Paulo Coelho - Al cincelea munte 1.0 '{Literatura}.docx
Paulo Coelho - Aleph 2.0 '{Literatura}.docx
Paulo Coelho - Brida 2.0 '{Literatura}.docx
Paulo Coelho - Diavolul si domnisoara Prym 0.99 '{Literatura}.docx
Paulo Coelho - Hippie 1.0 '{Literatura}.docx
Paulo Coelho - Invingatorul este intotdeauna singur 0.7 '{Literatura}.docx
Paulo Coelho - Jurnalul unui mag 1.0 '{Literatura}.docx
Paulo Coelho - La raul Piedra am sezut jos si am plans 0.99 '{Literatura}.docx
Paulo Coelho - Maktub 0.99 '{Literatura}.docx
Paulo Coelho - Manualul razboinicului luminii 0.99 '{Literatura}.docx
Paulo Coelho - Manuscrisul gasit la Accra 2.0 '{Literatura}.docx
Paulo Coelho - Unsprezece minute 1.0 '{Literatura}.docx
Paulo Coelho - Veronika se hotaraste sa moara 2.0 '{Literatura}.docx
Paulo Coelho - Vrajitoarea din Portobello 1.0 '{Literatura}.docx
Paulo Coelho - Walkiriile 0.99 '{Literatura}.docx
Paulo Coelho - Zahir 1.0 '{Literatura}.docx

./Paul Stefanescu:
Paul Stefanescu - Istoria serviciilor secrete romanesti 0.9 '{Istorie}.docx

./Paul Sussman:
Paul Sussman - Armata lui Cambyses 2.0 '{AventuraIstorica}.docx
Paul Sussman - Labirintul lui Osiris 1.0 '{AventuraIstorica}.docx
Paul Sussman - Oaza ascunsa 1.0 '{AventuraIstorica}.docx
Paul Sussman - Secretul templului 1.0 '{AventuraIstorica}.docx

./Paul Theroux:
Paul Theroux - Bazarul pe roti. cu trenul prin Asia 1.0 '{Calatorii}.docx

./Paul Valery:
Paul Valery - Alfabet 0.99 '{Diverse}.docx
Paul Valery - Criza spiritului si alte eseuri 0.7 '{Eseu}.docx

./Paul White:
Paul White - Sub copacul Buyu 0.99 '{Diverse}.docx

./Paul Zarifopol:
Paul Zarifopol - Din registrul ideilor gingase 0.9 '{Filozofie}.docx
Paul Zarifopol - Pentru arta literara 1.0 '{Arta si Istorie}.docx

./Pavel & Anatoli Sudoplatov:
Pavel & Anatoli Sudoplatov - Misiuni speciale 2.0 '{Istorie}.docx

./Pavel Amnuel:
Pavel Amnuel - Si am auzit glasul 0.99 '{Diverse}.docx

./Pavel Corut:
Pavel Corut - Calauza viitorului 0.7 '{ActiuneComando}.docx
Pavel Corut - Cartea creatorilor - Sute de retete concrete pentru facut bani 0.6 '{DezvoltarePersonala}.docx
Pavel Corut - Catre culmile succesului 0.8 '{DezvoltarePersonala}.docx
Pavel Corut - Dumnezeu nu foloseste armament psihotronic 0.9 '{Psihologie}.docx
Pavel Corut - Ghidul succesului 0.99 '{DezvoltarePersonala}.docx
Pavel Corut - Mantuirea de dupa cumplita ratacire 0.9 '{Diverse}.docx
Pavel Corut - Octogonul in actiune - V2 Fulgerul albastru 0.8 '{ActiuneComando}.docx
Pavel Corut - Octogonul in actiune - V3 Floarea de argint 0.8 '{ActiuneComando}.docx
Pavel Corut - Octogonul in actiune - V4 Balada lupului alb 1.2 '{ActiuneComando}.docx
Pavel Corut - Octogonul in actiune - V5 Dincolo de frontiere 0.8 '{ActiuneComando}.docx
Pavel Corut - Octogonul in actiune - V27 Razboiul zeilor 0.6 '{ActiuneComando}.docx
Pavel Corut - Octogonul in actiune - V83 Suflete de foc 0.6 '{ActiuneComando}.docx
Pavel Corut - Origini - V1 Codul lui Zamolxe 1.0 '{MistersiStiinta}.docx
Pavel Corut - Origini - V1 Luptele zeilor 0.8 '{MistersiStiinta}.docx
Pavel Corut - Samanta creatorilor V2 0.8 '{DezvoltarePersonala}.docx

./Pavel Vejinov:
Pavel Vejinov - Pieirea lui Aiax 1.0 '{SF}.docx

./Pearl S. Buck:
Pearl S. Buck - Casa de Lut - V1 Tarina 3.0 '{Literatura}.docx
Pearl S. Buck - Casa de Lut - V2 Feciorii 4.0 '{Literatura}.docx
Pearl S. Buck - Casa de Lut - V3 Casa invrajbita 4.0 '{Literatura}.docx
Pearl S. Buck - Casa de lut 4.0 '{Literatura}.docx
Pearl S. Buck - Exilata 2.0 '{Literatura}.docx
Pearl S. Buck - Inger luptator 2.0 '{Literatura}.docx
Pearl S. Buck - Mama 2.0 '{Literatura}.docx
Pearl S. Buck - Mandrie 0.8 '{Literatura}.docx
Pearl S. Buck - Patriotul 2.0 '{Literatura}.docx
Pearl S. Buck - Pavilionul femeilor 2.0 '{Literatura}.docx
Pearl S. Buck - Promisiunea 2.0 '{Literatura}.docx
Pearl S. Buck - Vant de rasarit, vant de la apus 3.0 '{Literatura}.docx

./Pedro A. de Alarcon:
Pedro A. de Alarcon - Femeia uriasa 1.0 '{Teroare}.docx

./Pedro Almodovar:
Pedro Almodovar - Patty Diphusa 0.8 '{Diverse}.docx

./Pedro Calderon de la Barca:
Pedro Calderon de la Barca - Alcadele din Zalameea 1.0 '{Teatru}.docx
Pedro Calderon de la Barca - Doamna nevazuta 1.0 '{Teatru}.docx
Pedro Calderon de la Barca - Numele cu folos dar si cu ponos 1.0 '{Teatru}.docx
Pedro Calderon de la Barca - Nu totdeauna ceea ce-i mai rau e si adevarat 1.0 '{Teatru}.docx
Pedro Calderon de la Barca - Viata e vis 1.0 '{Teatru}.docx

./Pedro Galvez:
Pedro Galvez - Testamentul lui Seneca 1.0 '{AventuraIstorica}.docx

./Pedro Gonzales Calero:
Pedro Gonzales Calero - Filozofia pentru bufoni 1.0 '{Filozofie}.docx

./Pedro Gutierrez:
Pedro Gutierrez - Trilogie murdara la Havana 0.99 '{Literatura}.docx

./Peggy Dern:
Peggy Dern - Karen 0.99 '{Dragoste}.docx
Peggy Dern - Secretul Cristinei 0.9 '{Romance}.docx

./Pelham Grenville Wodehouse:
Pelham Grenville Wodehouse - Castelul Blandings - V1 Fulger in plina vara 1.0 '{Politista}.docx
Pelham Grenville Wodehouse - Castelul Blandings - V2 Nori grei deasupra castelului Blandings 1.0 '{Politista}.docx
Pelham Grenville Wodehouse - Codul de onoare al Woosterilor 0.8 '{Politista}.docx
Pelham Grenville Wodehouse - Jeeves intra in actiune 1.0 '{Politista}.docx
Pelham Grenville Wodehouse - Jeeves si spiritul feudal 1.0 '{Politista}.docx
Pelham Grenville Wodehouse - S-a facut, Jeeves 0.8 '{Politista}.docx
Pelham Grenville Wodehouse - Sezonul de imperechere 1.0 '{Politista}.docx
Pelham Grenville Wodehouse - Un pelican la castelul Blandings 1.0 '{Politista}.docx

./Penelope Douglas:
Penelope Douglas - Intimidare 1.0 '{Romance}.docx

./Penelope Field:
Penelope Field - Cineva te urmareste 0.99 '{Dragoste}.docx
Penelope Field - Sa uitam sudul 0.7 '{Dragoste}.docx

./Penelope Fitzgerald:
Penelope Fitzgerald - Floarea albastra 1.0 '{Literatura}.docx

./Penny Jordan:
Penny Jordan - Captiva dorintei 1.0 '{Romance}.docx
Penny Jordan - Casatorie din interes 1.0 '{Romance}.docx
Penny Jordan - Dorinta de razbunare 0.9 '{Romance}.docx
Penny Jordan - Dorinta si iubire 0.9 '{Romance}.docx
Penny Jordan - Fiica lui Hassan 1.0 '{Romance}.docx
Penny Jordan - Libertatea de a iubi 0.9 '{Romance}.docx

./Penny Mccusker:
Penny Mccusker - Trepte spre cer 0.99 '{Dragoste}.docx

./Penny Osborne:
Penny Osborne - Albastru caraibian 0.99 '{Romance}.docx

./Percy Shelley:
Percy Shelley - Prometeu descatusat 1.0 '{Teatru}.docx

./Per Olof Ekstrom:
Per Olof Ekstrom - N-a dansat decat o vara 1.0 '{Dragoste}.docx

./Per Petterson:
Per Petterson - La furat de cai 0.9 '{Literatura}.docx

./Per Wahloo:
Per Wahloo - Crima de la etajul 31 1.0 '{Politista}.docx

./Peter Beagle:
Peter Beagle - Ultima licorna 1.0 '{Supranatural}.docx

./Peter Brett:
Peter Brett - Demon - V4 Tronul de cranii 1.0 '{Diverse}.docx

./Peter Cartur:
Peter Cartur - Ceata 0.99 '{SF}.docx

./Peter Chambers:
Peter Chambers - Nimeni nu moare de doua ori 1.0 '{Politista}.docx
Peter Chambers - Recviem pentru o roscata 1.0 '{Politista}.docx
Peter Chambers - Vorbeste-i de rau pe morti 2.0 '{Politista}.docx

./Peter Cheyney:
Peter Cheyney - Ce le pasa damelor 1.0 '{Politista}.docx
Peter Cheyney - Este randul tau micuto 0.9 '{Politista}.docx
Peter Cheyney - Marea teapa 1.0 '{Politista}.docx
Peter Cheyney - Misiunea cea mare 1.0 '{Politista}.docx
Peter Cheyney - Mister Callaghan 1.0 '{Politista}.docx
Peter Cheyney - Ultimul pahar 1.0 '{Politista}.docx

./Peter Collett:
Peter Collett - Cartea gesturilor 0.8 '{Psihologie}.docx

./Peter Cummings:
Peter Cummings - Crima care nu-mi iese din cap 1.0 '{Politista}.docx

./Peter Deunov:
Peter Deunov - Arta sfanta a nutritiei 1.0 '{Spiritualitate}.docx
Peter Deunov - Cartea rugaciunilor 1.0 '{Spiritualitate}.docx
Peter Deunov - Cuvintele sfinte ale maestrului 1.0 '{Spiritualitate}.docx
Peter Deunov - Ghid spiritual al sanatatii 1.0 '{Spiritualitate}.docx
Peter Deunov - Igiena sufletului si legile maestrului 1.0 '{Spiritualitate}.docx
Peter Deunov - In imparatia naturii vii 1.0 '{Spiritualitate}.docx
Peter Deunov - Invatatura maestrului Peter Deunov 1.0 '{Spiritualitate}.docx
Peter Deunov - Izvorul binelui 1.0 '{Spiritualitate}.docx
Peter Deunov - O noua constiinta pentru femei - Secretul dezvaluit 1.0 '{Spiritualitate}.docx
Peter Deunov - Respiratia 1.0 '{Spiritualitate}.docx
Peter Deunov - Secretul celor trei lumi 1.0 '{Spiritualitate}.docx
Peter Deunov - Soapte din eternitate 1.0 '{Spiritualitate}.docx
Peter Deunov - Spirit sanatos in corp sanatos 1.0 '{Spiritualitate}.docx
Peter Deunov - Testamentul culorilor 1.0 '{Spiritualitate}.docx
Peter Deunov - V1 Epistola catre suflete 1.0 '{Spiritualitate}.docx
Peter Deunov - V2 Conferinte 1914-1917 1.0 '{Spiritualitate}.docx
Peter Deunov - V3 Conferinte 1918-1919 1.0 '{Spiritualitate}.docx
Peter Deunov - V4 Conferinte 1920 1.0 '{Spiritualitate}.docx

./Peter Esterhazi:
Peter Esterhazi - Harmonia Caelestis 0.9 '{Diverse}.docx
Peter Esterhazi - O femeie 0.9 '{Necenzurat}.docx

./Peter F. Hamilton:
Peter F. Hamilton - Alegere profitabila 1.0 '{SF}.docx
Peter F. Hamilton - Commonwealth Saga - V1 Steaua Pandorei 2.0 '{Necenzurat}.docx
Peter F. Hamilton - Commonwealth Saga - V2 Judas unchained 2.0 '{SF}.docx
Peter F. Hamilton - Golul - V1 Golul visator 1.0 '{SF}.docx
Peter F. Hamilton - Golul - V2 Golul temporal V1 1.0 '{SF}.docx
Peter F. Hamilton - Golul - V2 Golul temporal V2 1.0 '{SF}.docx
Peter F. Hamilton - Golul - V3 Golul evolutiv V1 1.0 '{SF}.docx
Peter F. Hamilton - Golul - V3 Golul evolutiv V2 1.0 '{SF}.docx
Peter F. Hamilton - Involutie 0.99 '{SF}.docx
Peter F. Hamilton - Planeta moarta 0.99 '{SF}.docx
Peter F. Hamilton - Zorii Noptii - V1 Disfunctia realitatii 5.0 '{SF}.docx
Peter F. Hamilton - Zorii Noptii - V2 Alchimistul neutronic 4.0 '{SF}.docx
Peter F. Hamilton - Zorii Noptii - V3 Zeul adormit 3.0 '{SF}.docx

./Peter Gardos:
Peter Gardos - Febra in zori 1.0 '{Literatura}.docx

./Peter Heller:
Peter Heller - Constelatia cainelui 0.99 '{Literatura}.docx

./Peter James:
Peter James - Pe urmele mortului 1.0 '{Politista}.docx
Peter James - Sa mori frumos! 1.0 '{Politista}.docx

./Peter Kelder & Berni S. Sidgel:
Peter Kelder & Berni S. Sidgel - Stravechiul secret al tineretii vesnice V2 0.7 '{Sanatate}.docx

./Peter Mason:
Peter Mason - Asasin oficial 1.0 '{Suspans}.docx

./Peter Masters:
Peter Masters - Sapte semne sigure ale unei convertiri adevarate 0.9 '{Religie}.docx

./Peter Mayle:
Peter Mayle - Hotul de vinuri 1.0 '{Literatura}.docx
Peter Mayle - V1 Un an in Provence 1.0 '{Literatura}.docx
Peter Mayle - V2 din nou in Provence 1.0 '{Literatura}.docx
Peter Mayle - V3 Provence pentru totdeauna 1.0 '{Literatura}.docx

./Peter Moon & Reston B. Nicholas:
Peter Moon & Reston B. Nicholas - V1 Proiectul Montauk. experimente in timp 1.0 '{MistersiStiinta}.docx
Peter Moon & Reston B. Nicholas - V2 Montauk revizitat. Aventuri in sincronicitate 1.0 '{MistersiStiinta}.docx
Peter Moon & Reston B. Nicholas - V3 Piramidele din Montauk. O calatorie in universul constiintei 1.0 '{MistersiStiinta}.docx
Peter Moon & Reston B. Nicholas - V4 Conexiunea nazist tibetana cu proiectul Montauk 1.0 '{MistersiStiinta}.docx
Peter Moon & Reston B. Nicholas - V5 Intalnire in Pleiade 1.0 '{MistersiStiinta}.docx
Peter Moon & Reston B. Nicholas - V6 Montauk. Conexiunea extraterestra 1.0 '{MistersiStiinta}.docx
Peter Moon & Reston B. Nicholas - V7 Muzica timpului 0.99 '{MistersiStiinta}.docx

./Peter Nilson:
Peter Nilson - Paznicul universului 1.0 '{SF}.docx

./Peter Phillips:
Peter Phillips - Visele sunt sacre 6.0 '{SF}.docx

./Peter Robinson:
Peter Robinson - Dosarul cameleonul 1.0 '{Politista}.docx
Peter Robinson - Inainte de ispita 1.0 '{Politista}.docx
Peter Robinson - Prietena diavolului 1.0 '{Politista}.docx

./Peter Sloterdijk:
Peter Sloterdijk - Reguli pentru parcul uman 0.99 '{Filozofie}.docx

./Peter Spiegelman:
Peter Spiegelman - Pisica rosie 1.0 '{Literatura}.docx

./Peter Straub:
Peter Straub - Copiii pierduti 1.0 '{Horror}.docx

./Peter Swanson:
Peter Swanson - Cei care merita sa moara 1.0 '{Thriller}.docx
Peter Swanson - Fata cu un ceas in loc de inima 1.0 '{Literatura}.docx
Peter Swanson - O minciuna perfecta 1.0 '{Thriller}.docx

./Peter Tompkins & Christopher Bird:
Peter Tompkins & Christopher Bird - Viata secreta a plantelor 0.99 '{MistersiStiinta}.docx

./Peter V. Brett:
Peter V. Brett - Demon - V1 Omul pictat 1.0 '{SF}.docx
Peter V. Brett - Demon - V2 Sulita desertului 1.0 '{SF}.docx
Peter V. Brett - Demon - V3 Razboiul la lumina zilei 1.0 '{SF}.docx

./Petra Hammesfahr:
Petra Hammesfahr - Taina linistitului domn Genandy 0.9 '{Diverse}.docx

./Petra Sawley:
Petra Sawley - Umbrele trecutului 0.9 '{Dragoste}.docx

./Petre Barbu:
Petre Barbu - America 0.9 '{Diverse}.docx
Petre Barbu - Blazare 0.9 '{Diverse}.docx

./Petre Bellu:
Petre Bellu - Apararea are cuvantul 4.0 '{Thriller}.docx
Petre Bellu - Caine vagabond 1.0 '{Literatura}.docx
Petre Bellu - Cazul doamnei Predescu 1.0 '{Thriller}.docx
Petre Bellu - O crima langa Brasov 1.0 '{Thriller}.docx

./Petre Catalin Logofatu:
Petre Catalin Logofatu - Generatia Genius 0.9 '{Diverse}.docx
Petre Catalin Logofatu - Mortul care umbla 0.9 '{Diverse}.docx

./Petre Craciun:
Petre Craciun - Alarma la Peles 1.0 '{Politista}.docx

./Petre Cubin:
Petre Cubin - Proiect de piatra funerara 0.99 '{ProzaScurta}.docx

./Petre Fluerasu:
Petre Fluerasu - Casa Bonita 1.0 '{ProzaScurta}.docx
Petre Fluerasu - Lacul negru 1.0 '{ProzaScurta}.docx

./Petre Ispirescu:
Petre Ispirescu - Basme 1.0 '{BasmesiPovesti}.docx
Petre Ispirescu - Istoria lui Stefan Voda cel Mare si cel Bun 1.0 '{AventuraIstorica}.docx
Petre Ispirescu - Vlad Voda Tepes 1.0 '{AventuraIstorica}.docx

./Petre Luscalov:
Petre Luscalov - Extraordinarele peripetii ale lui Scatiu si Babusca 1.0 '{BasmesiPovesti}.docx
Petre Luscalov - Fiul muntilor 1.0 '{AventuraTineret}.docx

./Petre P. Carp:
Petre P. Carp - Romania si razboiul european 0.9 '{Politica}.docx

./Petre Pandrea:
Petre Pandrea - Calugarul alb 0.8 '{Religie}.docx

./Petre Salcudeanu:
Petre Salcudeanu - Biblioteca din Alexandria 1.0 '{Literatura}.docx
Petre Salcudeanu - Paznic la portile raiului 1.0 '{Literatura}.docx
Petre Salcudeanu - Sauna 1.0 '{Literatura}.docx

./Petre Tutea:
Petre Tutea - Filozofia nuantelor 0.99 '{Eseu}.docx

./Petre Varlan:
Petre Varlan - Infrangerea lui Thanatos 1.0 '{Politista}.docx

./Petru Botsi:
Petru Botsi - Portretul unui sfant - Ioan de Kronstadt 1.0 '{Religie}.docx

./Petru C. Baciu:
Petru C. Baciu - Rastigniri ascunse 0.8 '{Politica}.docx

./Petru Cimpoesu:
Petru Cimpoesu - Christina domestica si vanatorii de suflete 1.0 '{Diverse}.docx
Petru Cimpoesu - Erou fara voie 0.99 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V1 S-a intamplat in Abraxa 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V2 Galapagos 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V3 Noul judecator de instructie 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V4 Doctorul si motanul 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V5 Cortul rosu 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V6 Oda contelui 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V7 Demonul amiezii 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V8 Cartaphilus 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V9 Drumul spre imas 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V10 Zilele egiptiace 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V11 Ai un melc pe obraz 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V12 Cine citeste sa inteleaga 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand - V13 Ultimul privind atent spre sine 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V1 Tahigrama primei audiente 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V2 A doua audienta 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V3 A treia audienta si cea mai scurta 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V4 A patra audienta 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V5 Audienta a cincea 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V6 Vorbeste mai intai judecatorul de instructie 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V7 Comisarul relateaza 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V8 Tahigrama unei audiente 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V9 Comisarul vorbeste despre scrisoarea primita de la doctor 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V10 Intors in catacomba lui 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V11 Scrisoarea din cafenea 1.0 '{Diverse}.docx
Petru Cimpoesu - Povestea Marelui Brigand verso - V12 Ultima intalnire cu cainele tandru 1.0 '{Diverse}.docx
Petru Cimpoesu - Simion liftnicul 0.9 '{BasmesiPovesti}.docx

./Petru Dumitriu:
Petru Dumitriu - Cronica de familie V1 1.0 '{Literatura}.docx
Petru Dumitriu - Cronica de familie V2 1.0 '{Literatura}.docx
Petru Dumitriu - Cronica de familie V3 1.0 '{Literatura}.docx

./Petru Poanta:
Petru Poanta - Opera lui George Cosbuc 0.9 '{Critica}.docx

./Petru Popescu:
Petru Popescu - Copiii Domnului 1.0 '{Dragoste}.docx
Petru Popescu - Fata din Nazaret 1.0 '{AventuraIstorica}.docx
Petru Popescu - Prins 1.0 '{Dragoste}.docx
Petru Popescu - Revelatie pe Amazon 0.99 '{AventuraTineret}.docx
Petru Popescu - Supleantul 1.0 '{Dragoste}.docx
Petru Popescu - Urme in timp 1.0 '{AventuraTineret}.docx
Petru Popescu - Virtuti stramosesti 0.7 '{IstoricaRo}.docx

./Petru Popovici:
Petru Popovici - Biblia este totusi adevarata 0.9 '{Religie}.docx
Petru Popovici - Esti sigur 0.8 '{Religie}.docx
Petru Popovici - Formarea caracterului 0.8 '{Religie}.docx
Petru Popovici - Graiul martirilor crestini din primele secole 0.99 '{Religie}.docx
Petru Popovici - Lumini peste veacuri 0.8 '{Religie}.docx
Petru Popovici - Mari amagiri 0.9 '{Religie}.docx
Petru Popovici - Pot sa cred in Isus 0.9 '{Religie}.docx

./Petru Rezus:
Petru Rezus - Cositele doamnei 1.0 '{IstoricaRo}.docx

./Petru Suciu:
Petru Suciu - Metode psihoterapeutice in pateric 0.9 '{Psihologie}.docx

./Petru Vintila:
Petru Vintila - Calatorie pe planeta Zeta 1.0 '{SF}.docx

./Philip Corso:
Philip Corso - Dupa Roswell 0.8 '{MistersiStiinta}.docx

./Philip Jose Farmer:
Philip Jose Farmer - Fiica 0.99 '{SF}.docx
Philip Jose Farmer - Lumea Fluviului - V1 Inapoi la trupurile voastre razletite 3.0 '{SF}.docx
Philip Jose Farmer - Lumea Fluviului - V2 Vasul miraculos 3.0 '{SF}.docx
Philip Jose Farmer - Lumea Fluviului - V3 Planul misterios 3.0 '{SF}.docx
Philip Jose Farmer - Lumea Fluviului - V4 Labirintul magic 3.0 '{SF}.docx
Philip Jose Farmer - Lumea Fluviului - V5 Zeii lumii fluviului 3.0 '{SF}.docx
Philip Jose Farmer - Marti oameni sunt sparti 0.99 '{SF}.docx
Philip Jose Farmer - Nu spalati caratele 0.99 '{SF}.docx
Philip Jose Farmer - Poarta 0.99 '{SF}.docx
Philip Jose Farmer - Regele animalelor 0.99 '{SF}.docx
Philip Jose Farmer - V1 Dayworld 1.0 '{SF}.docx
Philip Jose Farmer - V2 Rebelul din Dayworld 1.0 '{SF}.docx
Philip Jose Farmer - V3 Dayworld Terminus 1.0 '{SF}.docx
Philip Jose Farmer - Venus iesind din valuri 1.0 '{SF}.docx

./Philip K. Dick:
Philip K. Dick - Ajustorii 0.99 '{SF}.docx
Philip K. Dick - Cele trei stigmate ale lui Palmer Eldritch 1.0 '{SF}.docx
Philip K. Dick - Clanurile de pe Alpha 1.0 '{SF}.docx
Philip K. Dick - Curgeti, lacrimile mele, zise politistul 1.0 '{SF}.docx
Philip K. Dick - Dr. Bloodmoney 1.0 '{SF}.docx
Philip K. Dick - Foster esti mort! 2.0 '{SF}.docx
Philip K. Dick - Furnica electrica 2.0 '{SF}.docx
Philip K. Dick - Invazia divina 1.0 '{SF}.docx
Philip K. Dick - Jocul de-a razboiul 0.99 '{SF}.docx
Philip K. Dick - Jucatorii de pe Titan 1.0 '{SF}.docx
Philip K. Dick - Labirintul mortii 1.0 '{SF}.docx
Philip K. Dick - Loterie solara 1.0 '{SF}.docx
Philip K. Dick - Navetistul 0.99 '{SF}.docx
Philip K. Dick - Ne putem reaminti totul pentru dumneavoastra 0.99 '{SF}.docx
Philip K. Dick - Omul din castelul inalt 2.0 '{SF}.docx
Philip K. Dick - Substanta M 2.0 '{SF}.docx
Philip K. Dick - Timpul dezarticulat 1.0 '{SF}.docx
Philip K. Dick - Ubik 1.0 '{SF}.docx
Philip K. Dick - Viseaza androizii oi electrice 2.0 '{SF}.docx
Philip K. Dick - Wub 0.99 '{SF}.docx

./Philip Kerr:
Philip Kerr - Berlin Noir - V1 Toporasi de Martie 1.0 '{Thriller}.docx
Philip Kerr - Berlin Noir - V2 Criminalul din umbra 1.1 '{Thriller}.docx
Philip Kerr - Berlin Noir - V3 Recviem german 1.0 '{Thriller}.docx

./Philip Le Roy:
Philip Le Roy - Ultima arma 2.0 '{Thriller}.docx

./Philip Macdonald:
Philip Macdonald - Domeniul interzis 0.99 '{SF}.docx
Philip Macdonald - Odihneasca-se in pace! 2.0 '{Politista}.docx

./Philippa Gregory:
Philippa Gregory - Dinastia Tudor - V3 Surorile Boleyn 2.0 '{AventuraIstorica}.docx
Philippa Gregory - Dinastia Tudor - V4 Mostenirea Boleyn 2.0 '{AventuraIstorica}.docx
Philippa Gregory - Fiica eminentei cenusii 2.0 '{AventuraIstorica}.docx
Philippa Gregory - Razboiul celor doua roze - V1 Regina alba 1.0 '{AventuraIstorica}.docx
Philippa Gregory - Razboiul celor doua roze - V2 Regina rosie 2.0 '{AventuraIstorica}.docx
Philippa Gregory - Razboiul celor doua roze - V4 Fiica eminentei cenusii 2.0 '{AventuraIstorica}.docx

./Philippe Curval:
Philippe Curval - Enclava 1.1 '{SF}.docx
Philippe Curval - Resacul spatiului 1.0 '{SF}.docx

./Philip Pullman:
Philip Pullman - Materiile Intunecate - V1 Luminile nordului 1.0 '{AventuraTineret}.docx
Philip Pullman - Materiile Intunecate - V2 Pumnalul diafan 1.0 '{AventuraTineret}.docx
Philip Pullman - Materiile Intunecate - V3 Ocheanul de chihlimbar 1.0 '{AventuraTineret}.docx

./Philipp Vandemberg:
Philipp Vandemberg - Conjuratia sixtina 1.0 '{AventuraIstorica}.docx

./Philipp Vandenberg:
Philipp Vandenberg - A cincea evanghelie 1.0 '{AventuraIstorica}.docx
Philipp Vandenberg - Al optulea pacat 1.0 '{AventuraIstorica}.docx
Philipp Vandenberg - Fauritorul de oglinzi 1.0 '{AventuraIstorica}.docx
Philipp Vandenberg - Gladiatorul 1.0 '{AventuraIstorica}.docx
Philipp Vandenberg - Pergamentul uitat 1.0 '{AventuraIstorica}.docx

./Philip Reeve:
Philip Reeve - Cronicile Oraselor Flamande - V1 Masinarii infernale 1.0 '{Diverse}.docx

./Philip Roth:
Philip Roth - Animal pe moarte 0.99 '{Literatura}.docx

./Philip Shelby:
Philip Shelby - Atac la presedinte 1.0 '{Thriller}.docx
Philip Shelby - Printre sacali 1.0 '{Thriller}.docx

./Phillip Mann:
Phillip Mann - Seniorul Paxwax 1.1 '{SF}.docx

./Phyllis A. Whitney:
Phyllis A. Whitney - Chihlimbarul negru 1.0 '{Thriller}.docx
Phyllis A. Whitney - Fantoma alba din Kyoto 0.9 '{Romance}.docx

./Phyllis Banks:
Phyllis Banks - Copilul de sambata 0.99 '{Dragoste}.docx
Phyllis Banks - O calatorie de vis 0.2 '{Dragoste}.docx

./Phyllis Dorothy James:
Phyllis Dorothy James - Farul 1.0 '{Politista}.docx
Phyllis Dorothy James - Fiul omului 1.0 '{Politista}.docx
Phyllis Dorothy James - Gustul mortii 1.0 '{Politista}.docx
Phyllis Dorothy James - Moartea unui expert 2.0 '{Politista}.docx
Phyllis Dorothy James - O meserie nepotrivita pentru o femeie 1.0 '{Politista}.docx

./Phyllis Whitney:
Phyllis Whitney - Chihlimbarul negru 1.0 '{Thriller}.docx

./Pierce Brown:
Pierce Brown - Red Rising - V1 Furia rosie 1.0 '{SF}.docx
Pierce Brown - Red Rising - V2 Furia aurie 1.0 '{SF}.docx
Pierce Brown - Red Rising - V3 Furia diminetii 1.0 '{SF}.docx
Pierce Brown - Red Rising - V4 Furia de fier 1.0 '{SF}.docx

./Pierre Accoce:
Pierre Accoce - Acesti bolnavi care ne guverneaza 1.0 '{Filozofie}.docx

./Pierre Adam:
Pierre Adam - Spre polul X 1.0 '{Tineret}.docx

./Pierre Albert & Andre Jean Tudesq:
Pierre Albert & Andre Jean Tudesq - Istoria Radio-Televiziunii 1.0 '{Istorie}.docx

./Pierre Andre Taguieff:
Pierre Andre Taguieff - Iluminatii 1.1 '{MistersiStiinta}.docx

./Pierre Barbet:
Pierre Barbet - Baphomet - V1 Imperiul lui Baphomet 2.0 '{SF}.docx
Pierre Barbet - Baphomet - V2 Cruciada stelara 2.0 '{SF}.docx
Pierre Barbet - Distrugeti Roma! 1.0 '{SF}.docx
Pierre Barbet - Mercenarii de pe Rychna 1.0 '{SF}.docx
Pierre Barbet - Nimfa spatiului 0.7 '{SF}.docx
Pierre Barbet - Planeta vrajita 1.1 '{SF}.docx

./Pierre Benoit:
Pierre Benoit - Koenigsmark. Atlantida 1.0 '{Literatura}.docx

./Pierre Boulle:
Pierre Boulle - O meserie de senior 0.7 '{AventuraTineret}.docx
Pierre Boulle - Planeta maimutelor 5.0 '{AventuraTineret}.docx
Pierre Boulle - Profesorul Mortimer 0.7 '{AventuraTineret}.docx

./Pierre Chenal:
Pierre Chenal - Ultima furtuna 1.0 '{Aventura}.docx

./Pierre Clostermann:
Pierre Clostermann - Marele circ 1.0 '{Aviatie}.docx

./Pierre de Boisdeffre:
Pierre de Boisdeffre - Dragostea si plictisul 1.0 '{Dragoste}.docx

./Pierre Demousson:
Pierre Demousson - Comoara idolului 1.0 '{Tineret}.docx
Pierre Demousson - Sclava piratului 1.0 '{Tineret}.docx

./Pierre Jean Brouillaud:
Pierre Jean Brouillaud - Monstrul si domnisoara de Crysale 0.7 '{SF}.docx
Pierre Jean Brouillaud - Piratii si poporul atolului de corali 0.7 '{SF}.docx

./Pierre Lemaitre:
Pierre Lemaitre - Alex 1.0 '{Politista}.docx
Pierre Lemaitre - Irene 1.0 '{Politista}.docx
Pierre Lemaitre - La revedere acolo sus 2.0 '{Politista}.docx

./Pierre Montet:
Pierre Montet - Egiptul pe vremea dinastiei Ramses 1.0 '{Istorie}.docx

./Pierre Moralie:
Pierre Moralie - Muntele tragic 0.9 '{Tineret}.docx

./Pierre Moustiers:
Pierre Moustiers - Iarna unui gentilom 1.0 '{AventuraIstorica}.docx

./Pierre Nemours:
Pierre Nemours - Detasamentul setei 1.0 '{ActiuneComando}.docx

./Pierre Stoltze:
Pierre Stoltze - Magicianul cosmosului 1.0 '{SF}.docx

./Pierre Stolze:
Pierre Stolze - Iubire stelara 1.0 '{SF}.docx

./Pierre Very:
Pierre Very - Cine l-a ucis pe Mos Craciun 1.0 '{Politista}.docx

./Piet Legay:
Piet Legay - Au fost saisprezece 1.0 '{ActiuneRazboi}.docx

./Pietro Aretino:
Pietro Aretino - Carti de joc vorbitoare 1.0 '{Teatru}.docx
Pietro Aretino - Comedia curtilor - Aretino 0.99 '{Teatru}.docx

./Pino Roveredo:
Pino Roveredo - Copile drag 0.99 '{Literatura}.docx

./Pintilie Iacob:
Pintilie Iacob - Vremuri de bajenie si surghiun 0.9 '{Biografie}.docx

./Pio Baroja:
Pio Baroja - Peripetiile lui Shanti Andia 1.0 '{Aventura}.docx

./Pittacus Lore:
Pittacus Lore - Mostenirile Lorienului - V1 Eu sunt numarul patru 1.0 '{SF}.docx
Pittacus Lore - Mostenirile Lorienului - V2 Puterea celor sase 1.0 '{SF}.docx
Pittacus Lore - Mostenirile Lorienului - V3 Lupta celor noua 1.0 '{SF}.docx
Pittacus Lore - Mostenirile Lorienului - V4 Greseala lui cinci 1.0 '{SF}.docx

./Pius Alibek:
Pius Alibek - Radacini nomade 0.9 '{Literatura}.docx

./Platon:
Platon - Alcibiade 1.0 '{Filozofie}.docx
Platon - Apararea lui Socrate 1.0 '{Filozofie}.docx
Platon - Charmides 1.0 '{Filozofie}.docx
Platon - Criton 1.0 '{Filozofie}.docx
Platon - Euthydemos 1.0 '{Filozofie}.docx
Platon - Euthyphron 1.0 '{Filozofie}.docx
Platon - Gorgias 1.0 '{Filozofie}.docx
Platon - Hippias Maior 1.0 '{Filozofie}.docx
Platon - Hippias Minor 1.0 '{Filozofie}.docx
Platon - Ion 1.0 '{Filozofie}.docx
Platon - Lahes 1.0 '{Filozofie}.docx
Platon - Lysis 1.0 '{Filozofie}.docx
Platon - Menexenos 1.0 '{Filozofie}.docx
Platon - Menon 1.0 '{Filozofie}.docx
Platon - Omul politic 1.0 '{Filozofie}.docx
Platon - Parmenide 1.0 '{Filozofie}.docx
Platon - Philebos 1.0 '{Filozofie}.docx
Platon - Protagoras 1.0 '{Filozofie}.docx
Platon - Republica. Mitul pesterii 1.0 '{Filozofie}.docx
Platon - Sofistul 1.0 '{Filozofie}.docx

./Plutarh:
Plutarh - Oameni ilustri ai Romei Antice 0.99 '{Filozofie}.docx

./Pompiliu Kostas Radulescu:
Pompiliu Kostas Radulescu - Zeul alb 0.9 '{ProzaScurta}.docx

./Pompiliu Tudoran:
Pompiliu Tudoran - In slujba domniei V1 1.1 '{IstoricaRo}.docx
Pompiliu Tudoran - In slujba domniei V2 1.1 '{IstoricaRo}.docx
Pompiliu Tudoran - In slujba domniei V3 1.1 '{IstoricaRo}.docx

./Ponson Du Terrail:
Ponson Du Terrail - Dramele Parisului - S1 Mostenirea misterioasa V1 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S1 Mostenirea misterioasa V2 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S2 Clubul valetilor de cupa V1 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S2 Clubul valetilor de cupa V2 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S2 Clubul valetilor de cupa V3 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S2 Clubul valetilor de cupa V4 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S3 Intrigile lui Rocambole - V1 O fiica a Spaniei 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S3 Intrigile lui Rocambole - V2 Moartea salbaticului 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S3 Intrigile lui Rocambole - V3 Razbunarea lui Baccarat 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S4 Cavalerii noptii - V1 Manuscrisul unei masti 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S4 Cavalerii noptii - V2 Ultima aparitie a lui Rocambole 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S4 Cavalerii noptii - V3 Testamentul lui Grain de Sel 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S5 Reinvierea lui Rocambole - V1 Inchisoarea din Toulon 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S5 Reinvierea lui Rocambole - V2 Orfanele 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S5 Reinvierea lui Rocambole - V3 Madeleine 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S5 Reinvierea lui Rocambole - V4 Subterana 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S6 Ultimul cuvant al lui Rocambole - V1 Distrugatorii 1.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S6 Ultimul cuvant al lui Rocambole - V2 Milioanele tigancii 1.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S6 Ultimul cuvant al lui Rocambole - V3 Frumoasa gradinareasa 1.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S6 Ultimul cuvant al lui Rocambole - V4 O drama in India 1.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S6 Ultimul cuvant al lui Rocambole - V5 Adevarul asupra lui Rocambole 1.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S7 Mizeriile Londrei - V1 Hoata de copii 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S7 Mizeriile Londrei - V2 Copilul pierdut 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S7 Mizeriile Londrei - V3 Newgate 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S7 Mizeriile Londrei - V4 Teribila drama 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S8 Ruinele Parisului - V1 Amorul zidarului 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S8 Ruinele Parisului - V2 Captivitatea stapanului 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S9 Funia spanzuratului - V1 Nebunul de la Bedlam 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Dramele Parisului - S9 Funia spanzuratului - V2 Omul gri 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V1.1 Frumoasa argintareasa 4.2 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V1.2 Amanta regelui Navarrei 4.2 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V1.3 Galanteriile frumoasei Nancy 4.2 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V2.1 Juramantul celor patru valeti V1 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V2.2 Amorurile valetului de trefla V2 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V3 Noaptea sfantului Bartolomeu 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V4.1 Regina baricadelor 3.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V4.2 Baricadele 3.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V5 Frumosul Galaor 3.0 '{CapasiSpada}.docx
Ponson Du Terrail - Junetea Regelui Henric - V6 A doua tinerete 3.0 '{CapasiSpada}.docx
Ponson Du Terrail - Noptile de la Maison Doree 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Parisul misterios - Spadasinii de la opera 2.0 '{CapasiSpada}.docx
Ponson Du Terrail - Tiganii Londrei - V1 Regele tiganilor 4.0 '{CapasiSpada}.docx
Ponson Du Terrail - Tiganii Londrei - V2 Regina tiganilor 4.0 '{CapasiSpada}.docx

./Poul Anderson:
Poul Anderson - Avatarul 0.99 '{SF}.docx
Poul Anderson - Dansatoarea din Atlantida 1.0 '{SF}.docx
Poul Anderson - Kyrie 0.9 '{SF}.docx
Poul Anderson - Operatiunea Haos 0.99 '{SF}.docx
Poul Anderson - Orion va rasari 0.99 '{SF}.docx
Poul Anderson - Patrula timpului 0.99 '{SF}.docx
Poul Anderson - Pazitorii timpului 1.0 '{CalatorieinTimp}.docx
Poul Anderson - Printre talhari 0.99 '{SF}.docx
Poul Anderson - Ziua reintoarcerii lor 1.0 '{SF}.docx

./Povestiri:
Poveste Japoneza - Palatul broastei testoase 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V01 4.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V02 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V03 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V04 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V05 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V06 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V07 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V08 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V09 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V10 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V11 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V12 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V13 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - BPT - V14 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V01 Noptile 001-024 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V02 Noptile 025-044 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V03 Noptile 045-129 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V04 Noptile 130-228 3.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V05 Noptile 230-315 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V06 Noptile 316-393 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V07 Noptile 394-479 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V08 Noptile 480-576 2.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V09 Noptile 577-671 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V10 Noptile 672-774 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V11 Noptile 775-819 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V12 Noptile 820-868 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V13 Noptile 869-922 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V14 Noptile 923-971 1.0 '{BasmesiPovesti}.docx
Povesti - 1001 de Nopti - Ercpress - V15 Noptile 972-1001 1.0 '{BasmesiPovesti}.docx
Povesti - Bulimandra si mandra lumei 1.0 '{BasmesiPovesti}.docx
Povesti - Calin nebunul 1.0 '{BasmesiPovesti}.docx
Povesti - Catana imparatului 1.0 '{BasmesiPovesti}.docx
Povesti - Ciobanasul cel istet si turloaiele blendei 1.0 '{BasmesiPovesti}.docx
Povesti - Craiasa zanelor 1.0 '{BasmesiPovesti}.docx
Povesti - Crancu, vanatorul codrului 1.0 '{BasmesiPovesti}.docx
Povesti - Curpan mare 1.0 '{BasmesiPovesti}.docx
Povesti - Doi feciori beti 1.0 '{BasmesiPovesti}.docx
Povesti - Dragan cenusa 1.0 '{BasmesiPovesti}.docx
Povesti - Fantana sticlisoarei 1.0 '{BasmesiPovesti}.docx
Povesti - Fat-Frumos cel ratacit 1.0 '{BasmesiPovesti}.docx
Povesti - Fiul iepei 1.0 '{BasmesiPovesti}.docx
Povesti - Gombei pasararul 1.0 '{BasmesiPovesti}.docx
Povesti - Greuceanu 1.0 '{BasmesiPovesti}.docx
Povesti - Ileana Samziana 1.0 '{BasmesiPovesti}.docx
Povesti - Imparatul impietrit 1.0 '{BasmesiPovesti}.docx
Povesti - Ion cel sarac si zana lacului 1.0 '{BasmesiPovesti}.docx
Povesti - Luceafarul de seara si luceafarul de zi 1.0 '{BasmesiPovesti}.docx
Povesti - Lupul cel nazdravan si Fat-Frumos 1.0 '{BasmesiPovesti}.docx
Povesti - Lupul cu cap de fier 1.0 '{BasmesiPovesti}.docx
Povesti - Mama Ciumii de la marginea lumii 1.0 '{BasmesiPovesti}.docx
Povesti - Mama zidita de vie 1.0 '{BasmesiPovesti}.docx
Povesti - Max si Moritz 1.0 '{BasmesiPovesti}.docx
Povesti - Minta-creata, busuioc si sucna-murga 1.0 '{BasmesiPovesti}.docx
Povesti - Moartea nase 1.0 '{BasmesiPovesti}.docx
Povesti - Noaptea cea lunga 1.0 '{BasmesiPovesti}.docx
Povesti - Omul de piatra 1.0 '{BasmesiPovesti}.docx
Povesti - Patania lui Dodo 0.7 '{BasmesiPovesti}.docx
Povesti - Pescarul si fiica regelui marii 1.0 '{BasmesiPovesti}.docx
Povesti - Pescarus imparatul 1.0 '{BasmesiPovesti}.docx
Povesti - Piciul ciobanasul si pomul cel fara capatai 1.0 '{BasmesiPovesti}.docx
Povesti - Piparus Petru si Florea Inflorit 1.0 '{BasmesiPovesti}.docx
Povesti - Povestea frumoasei Hacikazuki 1.0 '{BasmesiPovesti}.docx
Povesti - Povestea lui Furga Murga 1.0 '{BasmesiPovesti}.docx
Povesti - Povestea lupului nazdravan si a Ilenei Cosanzene 1.0 '{BasmesiPovesti}.docx
Povesti - Povestea tunetului Rai Taro 1.0 '{BasmesiPovesti}.docx
Povesti - Praslea cel voinic si merele de aur 1.0 '{BasmesiPovesti}.docx
Povesti - Stan bolovan 1.0 '{BasmesiPovesti}.docx
Povesti - Sur vultur 1.0 '{BasmesiPovesti}.docx
Povesti - Tara visului 1.0 '{BasmesiPovesti}.docx
Povesti - Taro cel de foc 1.0 '{BasmesiPovesti}.docx
Povesti - Tei leganat 1.0 '{BasmesiPovesti}.docx
Povesti - Tinerete fara batranete si viata fara de moarte 1.0 '{BasmesiPovesti}.docx
Povesti - Todoras purcarasu 1.0 '{BasmesiPovesti}.docx
Povesti - Trei copii saraci 1.0 '{BasmesiPovesti}.docx
Povesti - Tristi copil si Inia Dinia 1.0 '{BasmesiPovesti}.docx
Povesti - Tuliman 1.0 '{BasmesiPovesti}.docx
Povesti - Ulciorul fermecat 1.0 '{BasmesiPovesti}.docx
Povesti - Urashima Taro 1.0 '{BasmesiPovesti}.docx
Povesti - Urma Galbina si Piparus Petru 1.0 '{BasmesiPovesti}.docx
Povesti - Voinicul-Sarpe si fata de imparat 1.0 '{BasmesiPovesti}.docx
Povesti - Voinicul cel cu cartea in mana nascut 1.0 '{BasmesiPovesti}.docx
Povesti - Zana zorilor 1.0 '{BasmesiPovesti}.docx
Povesti Chinezesti - Gradina zeitei de jad 1.0 '{BasmesiPovesti}.docx
Povesti Chinezesti - Locuinta din gura tigrului 2.0 '{BasmesiPovesti}.docx
Povesti Chinezesti - Trei cazuri uluitoare 1.0 '{BasmesiPovesti}.docx
Povesti Japoneze - Cocorul alb 1.0 '{BasmesiPovesti}.docx
Povesti Japoneze - Tanarul Urashima 1.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 01-1966 4.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 02-1970 4.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 03-1994 3.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 04-1970 4.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 05-1967 2.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 06-1967 3.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 08-1974 3.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 10-1974 3.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 12-1975 3.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 13-1975 3.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 15-1972 2.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 16-1980 2.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 19 0.7 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 20-1977 1.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 21-1978 1.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 22-1980 1.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 23-1981 1.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 25-1983 1.0 '{BasmesiPovesti}.docx
Povesti Nemuritoare - Nr. 59-2001 1.0 '{BasmesiPovesti}.docx
Povesti populare chinezesti - Randunica guresa 1.0 '{BasmesiPovesti}.docx
Povestiri politiste - Antologia Enigma V1 1.0 '{Politista}.docx
Povestiri politiste - Antologia Enigma V2 1.0 '{Politista}.docx
Povestiri politiste - Crima care nu-mi iese din cap 1.0 '{Politista}.docx
Povestiri politiste - Crucea albastra 1.0 '{Politista}.docx
Povestiri S. F. Autori Romani - Fuga in spatiu timp 1.0 '{SF}.docx
Povestiri S. F. V1 0.99 '{SF}.docx
Povestiri S. F. V2 0.99 '{SF}.docx
Povestiri S. F. V3 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V01 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V02 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V03 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V04 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V05 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V06 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V07 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V08 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V09 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V10 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V11 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V12 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V13 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V14 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V15 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V16 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V17 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V18 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V19 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V20 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V21 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V22 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V23 1.0 '{SF}.docx
Povestiri Stiintifico Fantastice - V24 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V25 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V26 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V27 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V28 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V29 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V30 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V31 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V32 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V33 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V34 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V35 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V36 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V37 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V38 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V39 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V40 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V41 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V42 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V43 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V44 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V45 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V46 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V47 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V48 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V49 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V50 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V51 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V52 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V53 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V54 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V55 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V56 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V57 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V58 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V59 0.9 '{SF}.docx
Povestiri Stiintifico Fantastice - V60 0.9 '{SF}.docx
Povestiri Umoristice Chineze - Supa de ginseng 1.0 '{Umor}.docx
Povestiri Western - Cinci Povesti cu cowboy 2.0 '{Western}.docx
Povestiri Western - Goana dupa aur 1.1 '{Western}.docx
Povestiri Western - La capatul canionului 1.0 '{Western}.docx
Povestiri Western - Ursul din Arkansas 1.0 '{Western}.docx

./Prentice Mulford:
Prentice Mulford - In zarea nemuririi 1.0 '{Spiritualitate}.docx

./Profira Sadoveanu:
Profira Sadoveanu - Mormolocul 1.0 '{Literatura}.docx
Profira Sadoveanu - Rechinul 1.0 '{Literatura}.docx

./Prosper Merimee:
Prosper Merimee - Carmen 1.0 '{Dragoste}.docx
Prosper Merimee - Venus din Ille 0.9 '{Dragoste}.docx

./Proza scurta politista:
Proza scurta politista - Crucea albastra 2.0 '{Politista}.docx

./Publius Terentius Afer:
Publius Terentius Afer - Fata din Andros 0.99 '{Teatru}.docx

./Pu Sun Lin:
Pu Sun Lin - Patru povesti 0.99 '{BasmesiPovesti}.docx
```

